﻿# Russian Language Pack for Magento 2

Перевод интерфейса Magento 2 на русский язык.


# Installation

## Установка через composer.json:

Запустите следующую команду в корневой папке Magento 2

    composer require etws/magento-language-ru_ru:*
    
Не забудьте очистить кэш

    bin/magento cache:clean
    
Для некоторых фраз (в основном JS) помогает стирание статичных файлов
    
    rm -rf var/view_preprocessed/ pub/static/

## Другие способы установки
 
Инструкция по установке доступна на нашем сайте документации
[doc.etwebsolutions.com/ru/instruction/m2_russian_language_pack/install](http://doc.etwebsolutions.com/ru/instruction/m2_russian_language_pack/install)


# Помощь с переводом

Мы не профессиональные переводчики и поддерживаем перевод в силу своих возможностей и требований.
Если вы хотите помочь улучшении этого перевода, то перейдите по ссылке, чтобы узнать, как именно можете помочь.
[doc.etwebsolutions.com/ru/instruction/m2_russian_language_pack/i-want-to-help-you-with-the-translation](http://doc.etwebsolutions.com/ru/instruction/m2_russian_language_pack/i-want-to-help-you-with-the-translation)


# Готовность перевода

| | | |
|-|-|-|
|**Общий прогресс**|7310/9025|![Progress](http://progressed.io/bar/81)|

## Темы

| | | |
|-|-|-|
|frontend/Magento/luma|50/58|![Progress](http://progressed.io/bar/86)|
|frontend/Magento/blank|7/7|![Progress](http://progressed.io/bar/100)|

## Модули

| | | |
|-|-|-|
|Magento_AdminNotification|51/51|![Progress](http://progressed.io/bar/100)|
|Magento_AdvancedPricingImportExport|5/5|![Progress](http://progressed.io/bar/100)|
|Magento_Authorization|3/3|![Progress](http://progressed.io/bar/100)|
|Magento_Authorizenet|52/70|![Progress](http://progressed.io/bar/74)|
|Magento_Backend|440/494|![Progress](http://progressed.io/bar/89)|
|Magento_Backup|83/87|![Progress](http://progressed.io/bar/95)|
|Magento_Braintree|115/169|![Progress](http://progressed.io/bar/68)|
|Magento_Bundle|103/110|![Progress](http://progressed.io/bar/94)|
|Magento_Captcha|25/27|![Progress](http://progressed.io/bar/93)|
|Magento_CatalogImportExport|14/23|![Progress](http://progressed.io/bar/61)|
|Magento_CatalogInventory|66/78|![Progress](http://progressed.io/bar/85)|
|Magento_CatalogRule|86/89|![Progress](http://progressed.io/bar/97)|
|Magento_CatalogSearch|36/39|![Progress](http://progressed.io/bar/92)|
|Magento_CatalogUrlRewrite|6/6|![Progress](http://progressed.io/bar/100)|
|Magento_CatalogWidget|20/20|![Progress](http://progressed.io/bar/100)|
|Magento_Catalog|690/808|![Progress](http://progressed.io/bar/85)|
|Magento_CheckoutAgreements|40/41|![Progress](http://progressed.io/bar/98)|
|Magento_Checkout|177/199|![Progress](http://progressed.io/bar/89)|
|Magento_Cms|165/178|![Progress](http://progressed.io/bar/93)|
|Magento_Config|96/112|![Progress](http://progressed.io/bar/86)|
|Magento_ConfigurableProduct|109/161|![Progress](http://progressed.io/bar/68)|
|Magento_Contact|27/27|![Progress](http://progressed.io/bar/100)|
|Magento_Cookie|13/15|![Progress](http://progressed.io/bar/87)|
|Magento_Cron|19/22|![Progress](http://progressed.io/bar/86)|
|Magento_CurrencySymbol|20/22|![Progress](http://progressed.io/bar/91)|
|Magento_CustomerImportExport|19/20|![Progress](http://progressed.io/bar/95)|
|Magento_Customer|462/555|![Progress](http://progressed.io/bar/83)|
|Magento_Deploy|1/2|![Progress](http://progressed.io/bar/50)|
|Magento_Developer|9/10|![Progress](http://progressed.io/bar/90)|
|Magento_Dhl|41/83|![Progress](http://progressed.io/bar/49)|
|Magento_Directory|46/51|![Progress](http://progressed.io/bar/90)|
|Magento_DownloadableImportExport|5/5|![Progress](http://progressed.io/bar/100)|
|Magento_Downloadable|100/121|![Progress](http://progressed.io/bar/83)|
|Magento_Eav|92/145|![Progress](http://progressed.io/bar/63)|
|Magento_Email|92/110|![Progress](http://progressed.io/bar/84)|
|Magento_EncryptionKey|14/15|![Progress](http://progressed.io/bar/93)|
|Magento_Fedex|34/79|![Progress](http://progressed.io/bar/43)|
|Magento_GiftMessage|38/50|![Progress](http://progressed.io/bar/76)|
|Magento_GoogleAdwords|13/13|![Progress](http://progressed.io/bar/100)|
|Magento_GoogleOptimizer|7/8|![Progress](http://progressed.io/bar/88)|
|Magento_GroupedProduct|31/35|![Progress](http://progressed.io/bar/89)|
|Magento_ImportExport|91/121|![Progress](http://progressed.io/bar/75)|
|Magento_Indexer|20/25|![Progress](http://progressed.io/bar/80)|
|Magento_Integration|49/119|![Progress](http://progressed.io/bar/41)|
|Magento_LayeredNavigation|28/30|![Progress](http://progressed.io/bar/93)|
|Magento_Marketplace|15/15|![Progress](http://progressed.io/bar/100)|
|Magento_MediaStorage|21/26|![Progress](http://progressed.io/bar/81)|
|Magento_Msrp|16/18|![Progress](http://progressed.io/bar/89)|
|Magento_Multishipping|84/91|![Progress](http://progressed.io/bar/92)|
|Magento_Newsletter|150/153|![Progress](http://progressed.io/bar/98)|
|Magento_OfflinePayments|22/24|![Progress](http://progressed.io/bar/92)|
|Magento_OfflineShipping|53/57|![Progress](http://progressed.io/bar/93)|
|Magento_PageCache|11/21|![Progress](http://progressed.io/bar/52)|
|Magento_Payment|47/63|![Progress](http://progressed.io/bar/75)|
|Magento_Paypal|296/593|![Progress](http://progressed.io/bar/50)|
|Magento_Persistent|16/19|![Progress](http://progressed.io/bar/84)|
|Magento_ProductAlert|38/42|![Progress](http://progressed.io/bar/90)|
|Magento_ProductVideo|41/47|![Progress](http://progressed.io/bar/87)|
|Magento_Quote|49/64|![Progress](http://progressed.io/bar/77)|
|Magento_Reports|211/233|![Progress](http://progressed.io/bar/91)|
|Magento_Review|131/138|![Progress](http://progressed.io/bar/95)|
|Magento_Rss|8/8|![Progress](http://progressed.io/bar/100)|
|Magento_Rule|35/35|![Progress](http://progressed.io/bar/100)|
|Magento_SalesRule|155/167|![Progress](http://progressed.io/bar/93)|
|Magento_SalesSequence|2/2|![Progress](http://progressed.io/bar/100)|
|Magento_Sales|696/821|![Progress](http://progressed.io/bar/85)|
|Magento_Search|65/93|![Progress](http://progressed.io/bar/70)|
|Magento_Security|12/28|![Progress](http://progressed.io/bar/43)|
|Magento_SendFriend|50/50|![Progress](http://progressed.io/bar/100)|
|Magento_Shipping|160/185|![Progress](http://progressed.io/bar/86)|
|Magento_Sitemap|68/69|![Progress](http://progressed.io/bar/99)|
|Magento_Store|28/38|![Progress](http://progressed.io/bar/74)|
|Magento_Swatches|21/38|![Progress](http://progressed.io/bar/55)|
|Magento_TaxImportExport|20/20|![Progress](http://progressed.io/bar/100)|
|Magento_Tax|175/183|![Progress](http://progressed.io/bar/96)|
|Magento_Theme|170/203|![Progress](http://progressed.io/bar/84)|
|Magento_Translation|6/6|![Progress](http://progressed.io/bar/100)|
|Magento_Ui|98/126|![Progress](http://progressed.io/bar/78)|
|Magento_Ups|44/115|![Progress](http://progressed.io/bar/38)|
|Magento_UrlRewrite|61/63|![Progress](http://progressed.io/bar/97)|
|Magento_User|133/148|![Progress](http://progressed.io/bar/90)|
|Magento_Usps|44/133|![Progress](http://progressed.io/bar/33)|
|Magento_Variable|21/21|![Progress](http://progressed.io/bar/100)|
|Magento_Vault|9/13|![Progress](http://progressed.io/bar/69)|
|Magento_Webapi|25/28|![Progress](http://progressed.io/bar/89)|
|Magento_Weee|26/29|![Progress](http://progressed.io/bar/90)|
|Magento_Widget|70/72|![Progress](http://progressed.io/bar/97)|
|Magento_Wishlist|140/143|![Progress](http://progressed.io/bar/98)|
|Magento_GoogleAnalytics|5/5|![Progress](http://progressed.io/bar/100)|
|Magento_NewRelicReporting|12/21|![Progress](http://progressed.io/bar/57)|
|Magento_WebapiSecurity|2/2|![Progress](http://progressed.io/bar/100)|
|Magento_DesignEditor|2/2|![Progress](http://progressed.io/bar/100)|
|Magento_Log|2/2|![Progress](http://progressed.io/bar/100)|

## Библиотеки

| | | |
|-|-|-|
|lib/web/mage/adminhtml/backup.js|4/4|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/adminhtml/wysiwyg/widget.js|3/3|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/backend/suggest.js|3/3|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/decorate.js|1/1|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/dropdown.js|1/1|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/gallery/gallery.js|2/2|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/loader.js|2/2|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/loader_old.js|2/2|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/menu.js|2/2|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/translate-inline-vde.js|2/2|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/translate-inline.js|3/3|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/validation.js|4/4|![Progress](http://progressed.io/bar/100)|
|lib/web/mage/validation/validation.js|4/4|![Progress](http://progressed.io/bar/100)|