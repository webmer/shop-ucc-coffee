/**
 * Created by avolodin on 13.01.17.
 */

require.config({
    paths :{
        portfolio:   'js/portfolio/jquery.filterizr',
        pin:   'js/pin/jquery.pin.min',
        bootstrap: 'js/bootstrap/bootstrap.min',
        select2: 'js/select2/js/select2.full.min',
        product_slider: 'js/product_slider/bootstrap-slider.min',
        bxslider: 'js/bxslider/jquery.bxslider.min'
    },
    shim: {
        'portfolio' : {
            'deps': ['jquery']
        },
        'pin' : {
            'deps': ['jquery']
        },
        'product_slider' : {
            'deps': ['jquery']
        },
        'bootstrap' : {
            'deps': ['jquery']
        },
        'select2' : {
            'deps': ['jquery']
        },
        'bxslider' : {
            'deps': ['jquery']
        }
    }
});
require(["jquery","bootstrap","portfolio","pin","product_slider","bxslider", "select2"], function($){
    $(document).ready(function() {
        var body = $('body');
        // $('.bxslider').each(function(){
        //     var self = $(this);
        //     self.bxSlider({
        //         // minSlides: 4,
        //         // maxSlides: 4,
        //         // slideWidth: 130,
        //         // slideMargin: 10,
        //         controls: true,
        //         infiniteLoop: false
        //     });
        // });

        window.PLG = window.PLG || {};

        PLG.getNewId = (function(){
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return function(prefix){
                return (prefix||'')+s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
            }
        })();

        PLG.initToggle = (function(){

            return function(){
                $('[data-plg-toggle]').each(function(){
                    var button = $(this);
                    var id = PLG.getNewId();
                    var target = $(button.data('target'));
                    var plgHide = $(button.data('plg-hide'));
                    var isHideTarget = !!button.data('target-hide') && button.data('target-hide') == true;
                    if(isHideTarget){
                        target.hide();
                    }
                    
                    button.off('click').on('click',function(){                        
                        target.slideToggle("fast", function () {
                            if (plgHide.length) {
                                plgHide.slideToggle("fast");
                            }
                        });                        
                    });
                    button.attr('data-plg-toggle-id',id);
                });
            }
        })();

        // init

        PLG.initToggle();


        $("#mob-menu").click(function () {
            $("div.site-header_content").slideToggle('slow');
        });

        $(".header-content_items .brand-ul > li > a").on('click',function(e){
            $(".item-brands_popup").css({'opacity':'0','visibility':'hidden'});
            $(this).parents('li').first().find(".item-brands_popup").css({'opacity':'1','visibility':'visible'});
            e.preventDefault();
            return false;
        });

        $(".item-brands_close").click(function(e) {
            $(this).parent(".item-brands_popup").css({'opacity':'0','visibility':'hidden'});
            e.stopPropagation();
        });

        if($(".pinned").length) {
            $(".pinned").pin({
                containerSelector: ".site-content_wrapper",
                padding: {top: 100, bottom: 10}
            });
        }

        body.on('swatch.init', function(e,a){
            var intensitat = $(".intensitat");
            if(intensitat.length) {
                var input = intensitat.find('.swatch-input');
                var defaultValue = 1;
                var valBox = $('<div>').addClass('p_slider-value').text(defaultValue);
                intensitat.find('.swatch-attribute-options').css({height:0,overflow:'hidden',margin:0});
                var valueMapping = {};
                var ticks = [];
                var labels = [];
                intensitat.find('.swatch-option').each(function(){
                    var id = $(this).attr('option-id');
                    var value = $(this).attr('option-label');
                    valueMapping[id]=value;
                    ticks.push(id);
                    labels.push(value)
                });

                input.bootstrapSlider({
                    ticks: ticks,
                    ticks_labels: labels,
                    step: 1,
                    tooltip: 'hide'
                });

                input.before(valBox);

                // $("#p_slider").slider();
                input.on("slide", function (slideEvt) {
                    valBox.text(valueMapping[slideEvt.value]);

                });
            }
        });

        var inputs = $("input.qty_select2");
        if(inputs.length){
            inputs.each(function(){
                var input = $(this);
                var select = $('<select>');
                var step = input.attr('data-step');
                var count = 10;
                var template = input.attr('data-template');
                var options  = [];
                for(i=1;i<=count;i++){
                    var text = template.replace('%', (step*i));
                    var option = $('<option>').text(text).attr('value',i);
                    options.push(option)
                }
                select.append(options);
                select.on('change',function(){
                    var self = $(this);
                    var val = self.find(':selected').attr('value');
                    input.val(val);
                });
                input.hide().before(select);
                select.select2({
                    createTag: function (params) {

                        if($.isNumeric(params.term)){
                            var t = params.term * 1;
                            t = Math.round(t / 10) * 10;
                            var id = t /10;
                            t = template.replace('%', t);
                            return {
                                id: id,
                                text: t,
                                newOption: true
                            }
                        }
                        return false;
                    },
                    tags:true
                });

            });
        }


        //Default options
//         var options = {
//             // animationDuration: 0.5, //in seconds
//             // filter: 'all', //Initial filter
//             // callbacks: {
//             //     onFilteringStart: function() { },
//             //     onFilteringEnd: function() { },
//             //     onShufflingStart: function() { },
//             //     onShufflingEnd: function() { },
//             //     onSortingStart: function() { },
//             //     onSortingEnd: function() { }
//             // },
//             // delay: 0, //Transition delay in ms
//             // delayMode: 'progressive', //'progressive' or 'alternate'
//             // easing: 'ease-out',
//             // filterOutCss: { //Filtering out animation
//             //     opacity: 0,
//             //     transform: 'scale(0.5)'
//             // },
//             // filterInCss: { //Filtering in animation
//             //     opacity: 0,
//             //     transform: 'scale(1)'
//             // },
//             layout: 'sameWidth', //See layouts
//             selector: '.product-items',
//             setupControls: false
//         }
// //You can override any of these options and then call...
//         var filterizd = $('.product-items').filterizr(options);
//         if(filterizd && !!window.ias) {
//
//         window.ias.on('render', function(items){
//             // filterizd = $('.product-items').filterizr(options);
//             console.log('filterizd',filterizd);
//         });
//         window.ias.on('rendered', function(items){
//             filterizd = $('.product-items').filterizr(options);
//             console.log('filterizd',filterizd);
//         });
//         console.log('filterizd',filterizd);
//
//         $('.category-image-home').click(function() {
//             var targetFilter = $(this).data('multifltr');
//             if (targetFilter === 'all') {
//                 $('.category-image-home').removeClass('filtr-active');
//                 $(this).addClass('filtr-active');
//                 filterizd.filterizr('filter', 'all');
//                 filterizd._fltr._toggledCategories = { };
//                 console.log('all')
//             }
//             else {
//                 $('.category-image-home[data-multifltr="all"]').not(this).removeClass('filtr-active');
//                 $(this).toggleClass('filtr-active');
//                 // if(lastTarget){
//                 //     filterizd.filterizr('toggleFilter', lastTarget);
//                 // }
//                 filterizd.filterizr('toggleFilter', targetFilter);
//             }
//             if (!filterizd._fltr._multifilterModeOn()) {
//                 $('.category-image-home[data-multifltr="all"]').addClass('filtr-active');
//             }
//         });
//         }

    });
});