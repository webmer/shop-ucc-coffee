<?php
/**
 * Created by PhpStorm.
 * User: sefir_000
 * Date: 21.03.2017
 * Time: 14:42
 */

namespace PLG\CatalogWidget\Block\Product;

use Magento\CatalogWidget\Block\Product\ProductsList as CProductList;

class ProductsList extends CProductList
{
	protected $_storeManager;
	protected $_categoryHelper;
	protected $_categoryCollectionFactory;
	protected $_catalogSession;

	public function __construct(
        \Magento\Catalog\Model\Session $catalogSession,
		\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
		\Magento\Catalog\Helper\Category $categoryHelper,
		\Magento\Catalog\Block\Product\Context $context,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
		\Magento\Framework\App\Http\Context $httpContext,
		\Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
		\Magento\CatalogWidget\Model\Rule $rule,
		\Magento\Widget\Helper\Conditions $conditionsHelper,
        array $data
	)
	{
		$this->_storeManager = $context->getStoreManager();
		$this->_categoryHelper = $categoryHelper;
		$this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_catalogSession = $catalogSession;
		parent::__construct($context, $productCollectionFactory, $catalogProductVisibility, $httpContext, $sqlBuilder, $rule, $conditionsHelper, $data);
	}



	public function CategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
	{
		$collection = $this->_categoryCollectionFactory->create();
		$collection
            ->addAttributeToSelect('*')
            ->addAttributeToSelect('image')
            ->setStore($this->_storeManager->getStore());

		// select only active categories
		if ($isActive) {
			$collection->addIsActiveFilter();
		}

		// select categories of certain level
		if ($level) {
			$collection->addLevelFilter($level);
		}

		// sort categories by some value
		if ($sortBy) {
			$collection->addOrderField($sortBy);
		}

		// select certain number of categories
		if ($pageSize) {
			$collection->setPageSize($pageSize);
		}

		return $collection;
	}

	/**
	 * Retrieve current store categories
	 *
	 * @param bool|string $sorted
	 * @param bool $asCollection
	 * @param bool $toLoad
	 * @return \Magento\Framework\Data\Tree\Node\Collection or
	 * \Magento\Catalog\Model\ResourceModel\Category\Collection or array
	 */
	public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
	{
		return $this->_categoryHelper->getStoreCategories($sorted, $asCollection , $toLoad );
	}

    public function createCollection()
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter();
//            ->setPageSize($this->getPageSize())
//            ->setCurPage($this->getRequest()->getParam($this->getData('page_var_name'), 1));
        $collection->addAttributeToSelect('*');
        $conditions = $this->getConditions();
        $conditions->collectValidatedAttributes($collection);
        $this->sqlBuilder->attachConditionToCollection($collection, $conditions);

        return $collection;
    }

    public function setSessionData($key, $value)
    {
        return $this->_catalogSession->setData($key, $value);
    }

    public function getSessionData($key, $default = null, $remove = false)
    {
        $return = $this->_catalogSession->getData($key, $remove);
        if(empty($return)){
            $return = $default;
        }
        return $return;
    }
}