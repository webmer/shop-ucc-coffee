<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 15.03.17
 * Time: 17:40
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'PLG_PaymentRestriction',
    __DIR__
);