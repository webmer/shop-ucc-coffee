<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 19.05.17
 * Time: 18:10
 */


namespace PLG\Minicart\Plugin;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote\Item;

class DefaultItem
{

    protected $productRepo;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepo = $productRepository;
    }

    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $data = $proceed($item);

        /** @var Product $product */
        $product = $this->productRepo->getById($item->getProduct()->getId());
        $attributes = $product->getAttributes();
        $content = $attributes['content']->getFrontend()->getValue($product);
        $min_val = $attributes['min_val']->getFrontend()->getValue($product);
        $add_content = str_replace('%',$min_val, $content);
        $atts = [
            "product_content" => $content,
            "product_min_val" => $min_val,
            "product_add_content" => $add_content
        ];

        return array_merge($data, $atts);
    }
}