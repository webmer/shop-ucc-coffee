<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 05.05.17
 * Time: 15:28
 */

namespace PLG\CMS\Observer;

class ProductIsSalable implements \Magento\Framework\Event\ObserverInterface
{

    protected $_customerSession;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_customerSession = $customerSession;
    }


    public function isLogin()
    {
        return $this->_customerSession->isLoggedIn();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $observer->getSalable()->setIsSalable(false);
        if($observer->getProduct()->isAvailable() && $this->isLogin() ){
            $observer->getSalable()->setIsSalable(true);
        }

        return $this;
    }
}