<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 25.04.17
 * Time: 16:22
 */


namespace PLG\CMS\Block;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;

/**
 * Html page top menu block
 */
class CText extends Template
{
    public $before;
    public $tagWrapper = 'div';
    public $htmlClassWrapper;
    public $htmlIdWrapper;
    public $value;
    public $after;
}