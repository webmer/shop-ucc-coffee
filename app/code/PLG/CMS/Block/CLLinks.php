<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 25.04.17
 * Time: 16:22
 */


namespace PLG\CMS\Block;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;

/**
 * Html page top menu block
 */
class CLLinks extends Template
{
    protected $_customerUrl;
    protected $_customerSession;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Url $customerUrl,
        Template\Context $context,
        array $data
    )
    {
        $this->_customerUrl = $customerUrl;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    public function getCustomerLoginUrl() {
        return $this->_customerUrl->getLoginUrl();
    }
    public function getCustomerLogoutUrl() {
        return $this->_customerUrl->getLogoutUrl();
    }

    public function getCustomerRegisterUrl() {
        return $this->_customerUrl->getRegisterUrl();
    }

    public function getCustomerDashboardUrl()
    {
        return $this->_customerUrl->getDashboardUrl();
    }

    public function isLogin()
    {
        return $this->_customerSession->isLoggedIn();
    }
}