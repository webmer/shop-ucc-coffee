<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 25.04.17
 * Time: 16:22
 */


namespace PLG\CMS\Block;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Catalog\Model\Product;

class CAttr extends Template
{
    /**
     * @var Product
     */
    protected $_product = null;
    protected $_attribute = null;
    protected $_attributeCode = null;
    protected $_className = null;
    protected $_attributeValue = null;
    protected $_attributeAddAttribute = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    public function getAttributeCode()
    {
        if (!$this->_attributeCode) {
            $this->_attributeCode = $this->getAtCode();
        }
        return $this->_attributeCode;
    }

    public function getClassName()
    {
        if (!$this->_className) {
            $this->_className = $this->getCssClass();
        }
        return $this->_className;
    }

    public function getAttribute()
    {
        if (!$this->_attribute) {
            $this->_attribute = $this->getProduct()->getResource()->getAttribute($this->getAttributeCode());
        }
        return $this->_attribute;
    }

    public function getAttributeValue()
    {

        if (!$this->_attributeValue && $this->getAttribute()) {
            $this->_attributeValue = $this->getAttribute()->getFrontend()->getValue($this->getProduct());
        }
        return $this->_attributeValue;
    }

    public function getAttributeAddAttribute()
    {

        if (!$this->_attributeAddAttribute) {
            $this->_attributeAddAttribute = $this->getAddAttribute();
        }
        return $this->_attributeAddAttribute;
    }

    public function getAttributeLabel()
    {
        $atLabel = $this->getAtLabel();
        if($atLabel == 'default') {
            $atLabel = $this->getAttribute()->getStoreLabel();
        }
        return $atLabel;
    }
}