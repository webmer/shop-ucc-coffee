<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 25.04.17
 * Time: 16:22
 */


namespace PLG\Topmenu\Block\Html;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;

/**
 * Html page top menu block
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{


    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @param int $limit
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '', $limit = 0)
    {
        $this->_eventManager->dispatch(
            'page_block_html_topmenu_gethtml_before',
            ['menu' => $this->_menu, 'block' => $this]
        );
        $html = $this->_getHtml($this->_menu);

        $transportObject = new \Magento\Framework\DataObject(['html' => $html]);
        $this->_eventManager->dispatch(
            'page_block_html_topmenu_gethtml_after',
            ['menu' => $this->_menu, 'transportObject' => $transportObject]
        );
        $html = $transportObject->getHtml();
        return $html;
    }


    protected function _getHtml(
        \Magento\Framework\Data\Tree\Node $menuTree,
        $childrenWrapClass = '',
        $limit = 0,
        $colBrakes = []
    ) {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {
            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $classes = [];
            $image = $child->getImageUrl();
            $name = $this->escapeHtml($child->getName());
            $url = $child->getUrl();
            $subMenu = $this->_addSubMenu(
                $child,
                $childLevel
            );
            if($child->hasChildren()) {
                $classes[] = 'dropdown';
            }
            if ($child->getIsActive()) {
                $classes[] = 'active';
            } elseif ($child->getHasActive()) {
                $classes[] = 'active';//'has-active';
            }
            $classes = join(' ',$classes);
            $html .= <<<HTML
                <div class="navigation-items_item" style="background-image: url({$image});">
                    <div class="navigation-item_title">
                        <div class="item-title_block {$classes} transition">
                            <a href="{$url}">
                                {$name}
                            </a>
                        </div>
                        {$subMenu}
                    </div>
                </div>
HTML;


            $itemPosition++;
            $counter++;
        }

        return $html;
    }

    /**
     * Add sub menu HTML code for current menu item
     *
     * @param \Magento\Framework\Data\Tree\Node $child
     * @param string $childLevel
     * @param string $childrenWrapClass
     * @param int $limit
     * @return string HTML code
     */
    protected function _addSubMenu($child, $childLevel = 0, $childrenWrapClass = '', $limit = 0)
    {
        $html = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $items = [];
        foreach ($child->getChildren() as $children) {
            $class = $children->getUrlKey();
            $name = $this->escapeHtml($children->getName());
            $url = $children->getUrl();
            $items[] = "<li class=\"{$class}\">
                            <a href=\"{$url}\">
                                {$name}
                            </a>
                        </li>";
        }
        $itemsHtml = join('',$items);

        $html .= <<<HTML
        <div class="item-title_dropdown transition">
                    <ul>
                        {$itemsHtml}
                    </ul>
                </div>
HTML;

        return $html;
    }

}
