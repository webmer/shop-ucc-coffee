<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 03.04.17
 * Time: 17:16
 */

namespace PLG\Category\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{

    protected $categorySetupFactory;
    protected $eavSetupFactory;

    public function __construct(
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    )
    {
        $this->categorySetupFactory = $categorySetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;

    }


    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Category::ENTITY, 'plg_is_brand' );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY, 'plg_is_brand', [
                'type' => 'int',
                'label' => 'Include in Brands',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'default' => '1',
                'sort_order' => 15,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );
        $setup->endSetup();
    }
}