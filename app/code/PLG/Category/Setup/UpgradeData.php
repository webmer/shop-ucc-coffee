<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 12.04.17
 * Time: 11:16
 */

namespace PLG\Category\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{

    protected $categorySetupFactory;
    protected $eavSetupFactory;

    public function __construct(
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    )
    {
        $this->categorySetupFactory = $categorySetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;

    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
//        var_dump(version_compare($context->getVersion(), "1.0.2"), $context->getVersion());exit;
//        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
//        $categorySetup->removeAttribute(
//            \Magento\Catalog\Model\Category::ENTITY, 'is_brand' );

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Category::ENTITY, 'plg_is_brand' );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY, 'plg_is_brand', [
                'type' => 'int',
                'label' => 'Include in Brands',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'default' => '1',
                'sort_order' => 15,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );
        $setup->endSetup();
    }
}