<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 25.04.17
 * Time: 16:22
 */


namespace PLG\Banners\Block;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;

/**
 * Html page top menu block
 */
class Banner extends Template
{

    protected $bannersRepository;
    protected $registry;
    protected $_category;
    protected $_product;
    protected $_request;
    /**
     * @var \PLG\Banners\Model\ResourceModel\Banners\Collection
     */
    protected $bannersCollectionFactory;

    public function __construct(
        \PLG\Banners\Model\ResourceModel\Banners\CollectionFactory $bannersCollectionFactory,
        \Magento\Framework\App\Request\Http $request,
        \PLG\Banners\Api\BannersRepositoryInterface $bannersRepository,
        \Magento\Framework\Registry $registry,
        Template\Context $context,
        array $data
    )
    {
        $this->bannersRepository = $bannersRepository;
        $this->registry = $registry;
        $this->_request = $request;
        $this->bannersCollectionFactory     = $bannersCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getCurrentCatrgoty()
    {
        if(!$this->_category){
            $this->_category = $this->registry->registry('current_category');
        }
        return $this->_category;
    }

    public function getCurrentProduct()
    {
        if(!$this->_product){
            $this->_product= $this->registry->registry('product');
        }
        return $this->_product;
    }


    /**
     * @return \PLG\Banners\Model\Banners
     */
    public function getCurrentBanner()
    {
        $return = null;
        $now = new \DateTime();
        /**
         * @var $collection \PLG\Banners\Model\ResourceModel\Banners\Collection
         */

        if ($this->_request->getFullActionName() == 'catalog_product_view') {
            //you are on the product page

        }
        if ($this->_request->getFullActionName() == 'catalog_category_view') {
            //you are on the category page
            $category = $this->getCurrentCatrgoty();
            $collection = $this->bannersCollectionFactory->create();
            $collection->addFieldToFilter('type',['eq'=>$category->getLevel()]);
            $collection->addFieldToFilter('source',['eq'=>$category->getName()]);
            $collection->addFieldToFilter('is_active',['eq'=>true]);
            $collection->addFieldToFilter('from',[
                ['lteq'=>$now->format('Y-m-d')],
                ['null'=>'this_value_doesnt_matter']
            ]);
            $collection->addFieldToFilter('to',[
                ['gteq'=>$now->format('Y-m-d')],
                ['null'=>'this_value_doesnt_matter']
            ]);
            foreach ($collection as $banner) {
//                print_r($value);
                $return = $banner;
                break;
            }
            if(!$return){
                $currParent = null;
                foreach ($category->getParentCategories() as $parent) {
                    if($category->getId() != $parent->getId()){
                        $currParent = $parent;
                    }
                }
                $collection = $this->bannersCollectionFactory->create();

                if($currParent){
                    $collection->addFieldToFilter('type',['eq'=>$currParent->getLevel()]);
                    $collection->addFieldToFilter('source',['eq'=>$currParent->getName()]);
                    $collection->addFieldToFilter('is_active',['eq'=>true]);
                    $collection->addFieldToFilter('from',[
                        ['lteq'=>$now->format('Y-m-d')],
                        ['null'=>'this_value_doesnt_matter']
                    ]);
                    $collection->addFieldToFilter('to',[
                        ['gteq'=>$now->format('Y-m-d')],
                        ['null'=>'this_value_doesnt_matter']
                    ]);

                    foreach ($collection as $banner) {
        //                print_r($value);
                        $return = $banner;
                        break;
                    }
                }
            }
        }

        if ($this->_request->getFullActionName() == 'cms_index_index' || !$return) {
            //you are on the homepage
            $collection = $this->bannersCollectionFactory->create();

            $collection->addFieldToFilter('type',['eq'=>1]);
            $collection->addFieldToFilter('is_active',['eq'=>true]);
            $collection->addFieldToFilter('from',[
                ['lteq'=>$now->format('Y-m-d')],
                ['null'=>'this_value_doesnt_matter']
            ]);
            $collection->addFieldToFilter('to',[
                ['gteq'=>$now->format('Y-m-d')],
                ['null'=>'this_value_doesnt_matter']
            ]);
            echo '<pre style="display: none;">';
            echo (string) $collection->getSelect();
            echo '</pre>';
            foreach ($collection as $banner) {
//                print_r($value);
                $return = $banner;
                break;
            }
        }
//        $author = $this->authorRepository->getById($authorId);
        return $return;
    }
}