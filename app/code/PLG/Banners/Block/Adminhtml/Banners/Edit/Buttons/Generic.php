<?php
namespace PLG\Banners\Block\Adminhtml\Banners\Edit\Buttons;

use Magento\Backend\Block\Widget\Context;
use PLG\Banners\Api\BannersRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Generic
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var BannersRepositoryInterface
     */
    protected $bannersRepository;

    /**
     * @param Context $context
     * @param BannersRepositoryInterface $bannersRepository
     */
    public function __construct(
        Context $context,
        BannersRepositoryInterface $bannersRepository
    ) {
        $this->context = $context;
        $this->bannersRepository = $bannersRepository;
    }

    /**
     * Return Banners page ID
     *
     * @return int|null
     */
    public function getEntityId()
    {
        try {
            return $this->bannersRepository->getById(
                $this->context->getRequest()->getParam('entity_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
