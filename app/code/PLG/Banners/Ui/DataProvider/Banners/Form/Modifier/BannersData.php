<?php

namespace PLG\Banners\Ui\DataProvider\Banners\Form\Modifier;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use PLG\Banners\Model\ResourceModel\Banners\CollectionFactory;
use Magento\Ui\DataProvider\Modifier\Pool;

class BannersData extends Pool implements ModifierInterface
{
    /**
     * @var \Sample\News\Model\ResourceModel\Author\Collection
     */
    protected $collection;

    /**
     * @param CollectionFactory $authorCollectionFactory
     */
    public function __construct(
        CollectionFactory $authorCollectionFactory
    ) {
        $this->collection = $authorCollectionFactory->create();
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    /**
     * @param array $data
     * @return array|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function modifyData(array $data)
    {
        $items = $this->collection->getItems();
        /** @var $author \PLG\Banners\Model\Banners */
        foreach ($items as $author) {
            $_data = $author->getData();
            if (isset($_data['image'])) {
                $avatar = [];
                $avatar[0]['name'] = $author->getImage();
                $avatar[0]['url'] = $author->getImageUrl();
                $_data['image'] = $avatar;
            }
            $author->setData($_data);
            $data[$author->getId()] = $_data;
        }
        return $data;
    }
}
