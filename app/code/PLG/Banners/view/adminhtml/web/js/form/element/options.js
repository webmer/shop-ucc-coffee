/**
 * Created by avolodin on 09.05.17.
 */
define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'Magento_Ui/js/modal/modal'
], function ($,_, uiRegistry, select, modal) {
    'use strict';

    return select.extend({

        initialize: function () {
            this._super();

            this.fieldDepend(this.value());

            return this;
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {

            this.fieldDepend(this.value());

            return this._super();
        },

        fieldDepend: function(value){
            console.log('Select :'+value);
            var field1 = uiRegistry.get('index = source');
            var jField = $('#'+field1.uid);
            console.log(field1.hiddenValue, value);
            if (field1.hiddenValue != value) {
                field1.show();
                var selected = null;
                for(var i=0; i<field1.initialOptions.length; i++) {
                    var optionData = field1.initialOptions[i];
                    var option = jField.find('option[data-title="'+optionData['labeltitle']+'"]');
                    option.hide();
                    if(optionData['data-level'] == value){
                        if(!selected) selected = option;
                        option.show();
                    }
                }
                jField.val(selected.attr('value'));
            } else {
                field1.hide();
            }
        }
    });
});