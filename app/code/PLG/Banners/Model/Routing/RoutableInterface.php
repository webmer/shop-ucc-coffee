<?php
namespace PLG\Banners\Model\Routing;

interface RoutableInterface
{
    /**
     * @param $urlKey
     * @param $storeId
     * @return int|null
     */
    public function checkUrlKey($urlKey, $storeId);
}
