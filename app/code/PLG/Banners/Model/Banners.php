<?php
namespace PLG\Banners\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Data\Collection\Db;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use PLG\Banners\Api\Data\BannersInterface;
use PLG\Banners\Model\Banners\Url;
use PLG\Banners\Model\ResourceModel\Banners as BannersResourceModel;
use PLG\Banners\Model\Routing\RoutableInterface;
use PLG\Banners\Model\Source\AbstractSource;


/**
 * @method BannersResourceModel _getResource()
 * @method BannersResourceModel getResource()
 */
class Banners extends AbstractModel implements BannersInterface, RoutableInterface
{
    /**
     * @var int
     */
    const STATUS_ENABLED = 1;
    /**
     * @var int
     */
    const STATUS_DISABLED = 0;
    /**
     * @var Url
     */
    protected $urlModel;
    /**
     * cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'plg_banners_banners';

    /**
     * cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'plg_banners_banners';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'plg_banners_banners';

    /**
     * filter model
     *
     * @var \Magento\Framework\Filter\FilterManager
     */
    protected $filter;

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * @var \PLG\Banners\Model\Output
     */
    protected $outputProcessor;

    /**
     * @var AbstractSource[]
     */
    protected $optionProviders;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param Output $outputProcessor
     * @param UploaderPool $uploaderPool
     * @param FilterManager $filter
     * @param Url $urlModel
     * @param array $optionProviders
     * @param array $data
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Output $outputProcessor,
        UploaderPool $uploaderPool,
        FilterManager $filter,
        Url $urlModel,
        array $optionProviders = [],
        array $data = [],
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null
    )
    {
        $this->outputProcessor = $outputProcessor;
        $this->uploaderPool    = $uploaderPool;
        $this->filter          = $filter;
        $this->urlModel        = $urlModel;
        $this->optionProviders = $optionProviders;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(BannersResourceModel::class);
    }

    /**
     * Get in rss
     *
     * @return bool|int
     */
    public function getFrom()
    {
        return $this->getData(BannersInterface::FROM);
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->getData(BannersInterface::TYPE);
    }

    /**
     * Get awards
     *
     * @return string
     */
    public function getTo()
    {
        return $this->getData(BannersInterface::TO);
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getSource()
    {
        return $this->getData(BannersInterface::SOURCE);
    }

    /**
     * set name
     *
     * @param $name
     * @return BannersInterface
     */
    public function setName($name)
    {
        return $this->setData(BannersInterface::NAME, $name);
    }

    /**
     * Set in rss
     *
     * @param $inRss
     * @return BannersInterface
     */
    public function setFrom($from)
    {
        return $this->setData(BannersInterface::FROM, $from);
    }

    /**
     * Set biography
     *
     * @param $to
     * @return BannersInterface
     */
    public function setTo($to)
    {
        return $this->setData(BannersInterface::TO, $to);
    }

    /**
     * Set DOB
     *
     * @param $source
     * @return BannersInterface
     */
    public function setSource($source)
    {
        return $this->setData(BannersInterface::SOURCE, $source);
    }

    /**
     * set type
     *
     * @param $type
     * @return BannersInterface
     */
    public function setType($type)
    {
        return $this->setData(BannersInterface::TYPE, $type);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(BannersInterface::NAME);
    }

    /**
     * Get is active
     *
     * @return bool|int
     */
    public function getIsActive()
    {
        return $this->getData(BannersInterface::IS_ACTIVE);
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getImage()
    {
        return $this->getData(BannersInterface::IMAGE);
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getData(BannersInterface::URL);
    }

    /**
     * @return bool|string
     * @throws LocalizedException
     */
    public function getImageUrl()
    {
        $url = false;
        $avatar = $this->getImage();
        if ($avatar) {
            if (is_string($avatar)) {
                $uploader = $this->uploaderPool->getUploader('image');
                $url = $uploader->getBaseUrl().$uploader->getBasePath().$avatar;
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the avatar url.')
                );
            }
        }
        return $url;
    }

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(BannersInterface::CREATED_AT);
    }

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(BannersInterface::UPDATED_AT);
    }

    /**
     * Set is active
     *
     * @param $isActive
     * @return BannersInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(BannersInterface::IS_ACTIVE, $isActive);
    }

    /**
     * set avatar
     *
     * @param $image
     * @return BannersInterface
     */
    public function setImage($image)
    {
        return $this->setData(BannersInterface::IMAGE, $image);
    }

    /**
     * set url
     *
     * @param $url
     * @return BannersInterface
     */
    public function setUrl($url)
    {
        return $this->setData(BannersInterface::URL, $url);
    }

    /**
     * set created at
     *
     * @param $createdAt
     * @return BannersInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(BannersInterface::CREATED_AT, $createdAt);
    }

    /**
     * set updated at
     *
     * @param $updatedAt
     * @return BannersInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(BannersInterface::UPDATED_AT, $updatedAt);
    }


    /**
     * Check if banners url key exists
     * return banners id if banners exists
     *
     * @param string $urlKey
     * @param int $storeId
     * @return int
     */
    public function checkUrlKey($urlKey, $storeId)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $storeId);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $storeId
     * @return BannersInterface
     */
    public function setStoreId($storeId)
    {
        $this->setData(BannersInterface::STORE_ID, $storeId);
        return $this;
    }

    /**
     * @return array
     */
    public function getStoreId()
    {
        return $this->getData(BannersInterface::STORE_ID);
    }


    /**
     * sanitize the url key
     *
     * @param $string
     * @return string
     */
    public function formatUrlKey($string)
    {
        return $this->filter->translitUrl($string);
    }

    /**
     * @return mixed
     */
    public function getBannersUrl()
    {
        return $this->urlModel->getBannersUrl($this);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool)$this->getIsActive();
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeText($attribute)
    {
        if (!isset($this->optionProviders[$attribute])) {
            return '';
        }
        if (!($this->optionProviders[$attribute] instanceof AbstractSource)) {
            return '';
        }
        return $this->optionProviders[$attribute]->getOptionText($this->getData($attribute));
    }
}
