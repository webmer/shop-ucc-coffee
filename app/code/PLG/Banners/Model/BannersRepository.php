<?php
namespace PLG\Banners\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use PLG\Banners\Api\BannersRepositoryInterface;
use PLG\Banners\Api\Data;
use PLG\Banners\Api\Data\BannersInterface;
use PLG\Banners\Api\Data\BannersInterfaceFactory;
use PLG\Banners\Api\Data\BannersSearchResultsInterfaceFactory;
use PLG\Banners\Model\ResourceModel\Banners as ResourceBanners;
use PLG\Banners\Model\ResourceModel\Banners\Collection;
use PLG\Banners\Model\ResourceModel\Banners\CollectionFactory as BannersCollectionFactory;

/**
 * Class BannersRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class BannersRepository implements BannersRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];
    /**
     * @var ResourceBanners
     */
    protected $resource;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var BannersCollectionFactory
     */
    protected $bannersCollectionFactory;
    /**
     * @var BannersSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var BannersInterfaceFactory
     */
    protected $bannersInterfaceFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    public function __construct(
        ResourceBanners $resource,
        StoreManagerInterface $storeManager,
        BannersCollectionFactory $bannersCollectionFactory,
        BannersSearchResultsInterfaceFactory $bannersSearchResultsInterfaceFactory,
        BannersInterfaceFactory $bannersInterfaceFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->resource                 = $resource;
        $this->storeManager             = $storeManager;
        $this->bannersCollectionFactory  = $bannersCollectionFactory;
        $this->searchResultsFactory     = $bannersSearchResultsInterfaceFactory;
        $this->bannersInterfaceFactory   = $bannersInterfaceFactory;
        $this->dataObjectHelper         = $dataObjectHelper;
    }
    /**
     * Save page.
     *
     * @param \PLG\Banners\Api\Data\BannersInterface $banners
     * @return \PLG\Banners\Api\Data\BannersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(BannersInterface $banners)
    {
        /** @var BannersInterface|\Magento\Framework\Model\AbstractModel $banners */
        if (empty($banners->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $banners->setStoreId($storeId);
        }
        try {
            $this->resource->save($banners);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the entity: %1',
                $exception->getMessage()
            ));
        }
        return $banners;
    }

    /**
     * Retrieve Banners.
     *
     * @param int $bannersId
     * @return \PLG\Banners\Api\Data\BannersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($bannersId)
    {
        if (!isset($this->instances[$bannersId])) {
            /** @var \PLG\Banners\Api\Data\BannersInterface|\Magento\Framework\Model\AbstractModel $banners */
            $banners = $this->bannersInterfaceFactory->create();
            $this->resource->load($banners, $bannersId);
            if (!$banners->getId()) {
                throw new NoSuchEntityException(__('Requested entity doesn\'t exist'));
            }
            $this->instances[$bannersId] = $banners;
        }
        return $this->instances[$bannersId];
    }

    /**
     * Retrieve pages matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \PLG\Banners\Api\Data\BannersSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \PLG\Banners\Api\Data\BannersSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \PLG\Banners\Model\ResourceModel\Banners\Collection $collection */
        $collection = $this->bannersCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'entity_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \PLG\Banners\Api\Data\BannersInterface[] $bannerss */
        $banners = [];
        /** @var \PLG\Banners\Model\Banners $banner */
        foreach ($collection as $banner) {
            /** @var \PLG\Banners\Api\Data\BannersInterface $bannersDataObject */
            $bannerDataObject = $this->bannersInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($bannerDataObject, $banner->getData(), BannersInterface::class);
            $banners[] = $bannersDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($banners);
    }

    /**
     * Delete banners.
     *
     * @param \PLG\Banners\Api\Data\BannersInterface $banners
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(BannersInterface $banners)
    {
        /** @var \PLG\Banners\Api\Data\BannersInterface|\Magento\Framework\Model\AbstractModel $banners */
        $id = $banners->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($banners);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove entity %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete banners by ID.
     *
     * @param int $bannersId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($bannersId)
    {
        $banners = $this->getById($bannersId);
        return $this->delete($banners);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

}
