<?php
namespace PLG\Banners\Model\Source;

use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class Country extends AbstractSource implements ArrayInterface
{
    /**
     * @var CountryCollectionFactory
     */
    protected $countryCollectionFactory;

    protected $categoryCollectionFactory;

    protected $_storeManager;

    /**
     * @param CountryCollectionFactory $countryCollectionFactory
     * @param array $options
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        CountryCollectionFactory $countryCollectionFactory,
        array $options = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($options);
    }

    /**
     * get options as key value pair
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (empty($this->options) ) {
            $collection = $this->categoryCollectionFactory->create();
            $collection
                ->addAttributeToSelect('*')
//                ->addAttributeToSelect('image')
                ->setStore($this->_storeManager->getStore());

//            $this->options = [['label' => 'Default', 'value' => '']];

//            var_dump('PLG_TEST',count($collection));

            foreach ($collection as $category) {
                $this->options[$category->getName()] = [
                    'label' => $category->getName(),
                    'value' => $category->getName(),
                    'data-level' => $category->getLevel()
                ];
            }
        }
        return $this->options;
    }
}
