<?php
namespace PLG\Banners\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use PLG\Banners\Api\Data\BannersInterface;

/**
 * @api
 */
interface BannersRepositoryInterface
{
    /**
     * Save page.
     *
     * @param BannersInterface $banner
     * @return BannersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(BannersInterface $banner);

    /**
     * Retrieve Author.
     *
     * @param int $entityId
     * @return BannersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($entityId);

    /**
     * Retrieve pages matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Sample\News\Api\Data\AuthorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete author.
     *
     * @param BannersInterface $entity
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(BannersInterface $entity);

    /**
     * Delete author by ID.
     *
     * @param int $authorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($authorId);
}
