<?php
namespace PLG\Banners\Api\Data;

/**
 * @api
 */
interface BannersInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const AUTHOR_ID         = 'entity_id';
    const NAME              = 'name';
    const TYPE              = 'type';
    const IS_ACTIVE         = 'is_active';
    const SOURCE            = 'source';
    const FROM              = 'from';
    const TO                = 'to';
    const IMAGE             = 'image';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';
    const STORE_ID          = 'store_id';
    const URL               = 'url';


    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Get type
     *
     * @return int
     */
    public function getType();


    /**
     * Get is active
     *
     * @return bool|int
     */
    public function getIsActive();

    /**
     * Get is active
     *
     * @return string
     */
    public function getSource();

    /**
     * Get is active
     *
     * @return string
     */
    public function getFrom();

    /**
     * Get is active
     *
     * @return string
     */
    public function getTo();

    /**
     * Get is active
     *
     * @return string
     */
    public function getImage();

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl();


    /**
     * set id
     *
     * @param $id
     * @return BannersInterface
     */
    public function setId($id);

    /**
     * set name
     *
     * @param $name
     * @return BannersInterface
     */
    public function setName($name);

    /**
     * set type
     *
     * @param $type
     * @return BannersInterface
     */
    public function setType($type);

    /**
     * Set is active
     *
     * @param $isActive
     * @return BannersInterface
     */
    public function setIsActive($isActive);

    /**
     * Set in source
     *
     * @param $source
     * @return BannersInterface
     */
    public function setSource($source);

    /**
     * Set from
     *
     * @param $from
     * @return BannersInterface
     */
    public function setFrom($from);

    /**
     * Set to
     *
     * @param $to
     * @return BannersInterface
     */
    public function setTo($to);

    /**
     * set image
     *
     * @param $image
     * @return BannersInterface
     */
    public function setImage($image);

    /**
     * set url
     *
     * @param $url
     * @return BannersInterface
     */
    public function setUrl($url);


    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * set created at
     *
     * @param $createdAt
     * @return BannersInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * set updated at
     *
     * @param $updatedAt
     * @return BannersInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @param $storeId
     * @return BannersInterface
     */
    public function setStoreId($storeId);

    /**
     * @return int[]
     */
    public function getStoreId();

}
