<?php
namespace PLG\Banners\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * @api
 */
interface BannersSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get author list.
     *
     * @return \Sample\News\Api\Data\AuthorInterface[]
     */
    public function getItems();

    /**
     * Set authors list.
     *
     * @param \Sample\News\Api\Data\AuthorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
