<?php
namespace PLG\Banners\Controller;

class RegistryConstants
{
    /**
     * Registry key where current author ID is stored
     */
    const CURRENT_BANNERS_ID = 'current_banners_id';
}
