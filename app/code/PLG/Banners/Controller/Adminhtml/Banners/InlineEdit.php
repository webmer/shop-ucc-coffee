<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Framework\View\Result\PageFactory;
use PLG\Banners\Api\BannersRepositoryInterface;
use PLG\Banners\Api\Data\BannersInterface;
use PLG\Banners\Api\Data\BannersInterfaceFactory;
use PLG\Banners\Controller\Adminhtml\Banners as BannersController;
use PLG\Banners\Model\Banners;
use PLG\Banners\Model\ResourceModel\Banners as BannersResourceModel;

class InlineEdit extends BannersController
{
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var BannersResourceModel
     */
    protected $bannersResourceModel;

    /**
     * @param Registry $registry
     * @param BannersRepositoryInterface $bannersRepository
     * @param PageFactory $resultPageFactory
     * @param Date $dateFilter
     * @param Context $context
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonFactory $jsonFactory
     * @param BannersResourceModel $bannersResourceModel
     */
    public function __construct(
        Registry $registry,
        BannersRepositoryInterface $bannersRepository,
        PageFactory $resultPageFactory,
        Date $dateFilter,
        Context $context,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        JsonFactory $jsonFactory,
        BannersResourceModel $bannersResourceModel
    )
    {
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->jsonFactory         = $jsonFactory;
        $this->bannersResourceModel = $bannersResourceModel;
        parent::__construct($registry, $bannersRepository, $resultPageFactory, $dateFilter, $context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $bannersId) {
            /** @var \PLG\Banners\Model\Banners|BannersInterface $banners */
            $banners = $this->bannersRepository->getById((int)$bannersId);
            try {
                $bannersData = $this->filterData($postItems[$bannersId]);
                $this->dataObjectHelper->populateWithArray($banners, $bannersData , BannersInterface::class);
                $this->bannersResourceModel->saveAttribute($banners, array_keys($bannersData));
            } catch (LocalizedException $e) {
                $messages[] = $this->getErrorWithBannersId($banners, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithBannersId($banners, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithBannersId(
                    $banners,
                    __('Something went wrong while saving the banners.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add banners id to error message
     *
     * @param Banners $banners
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithBannersId(Banners $banners, $errorText)
    {
        return '[Banners ID: ' . $banners->getId() . '] ' . $errorText;
    }
}
