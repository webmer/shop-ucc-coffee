<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use PLG\Banners\Controller\Adminhtml\Banners;
use PLG\Banners\Controller\RegistryConstants;

class Edit extends Banners
{
    /**
     * Initialize current author and set it in the registry.
     *
     * @return int
     */
    protected function _initBanners()
    {
        $entityId = $this->getRequest()->getParam('entity_id');
        $this->coreRegistry->register(RegistryConstants::CURRENT_BANNERS_ID, $entityId);

        return $entityId;
    }

    /**
     * Edit or create author
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $entityId = $this->_initBanners();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('PLG_Banners::banners');
        $resultPage->getConfig()->getTitle()->prepend(__('Banners'));
        $resultPage->addBreadcrumb(__('PLG'), __('PLG'));
        $resultPage->addBreadcrumb(__('Banners'), __('Banners'), $this->getUrl('plg_banners/banners'));

        if ($entityId === null) {
            $resultPage->addBreadcrumb(__('New Banner'), __('New Banner'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Banner'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Banner'), __('Edit Banner'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->bannersRepository->getById($entityId)->getName()
            );
        }
        return $resultPage;
    }
}
