<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

class MassEnable extends MassDisable
{
    /**
     * @var bool
     */
    protected $isActive = true;
}
