<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use PLG\Banners\Model\Banners;

class MassDisable extends MassAction
{
    /**
     * @var bool
     */
    protected $isActive = false;

    /**
     * @param Banners $entity
     * @return $this
     */
    protected function massAction(Banners $entity)
    {
        $entity->setIsActive($this->isActive);
        $this->bannersRepository->save($entity);
        return $this;
    }
}
