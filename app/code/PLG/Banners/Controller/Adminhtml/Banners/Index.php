<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use \PLG\Banners\Controller\Adminhtml\Banners as BannersController;

class Index extends BannersController
{
    /**
     * Banners list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('PLG_Banners::banners');
        $resultPage->getConfig()->getTitle()->prepend(__('Banners'));
        $resultPage->addBreadcrumb(__('PLG'), __('PLG'));
        $resultPage->addBreadcrumb(__('Banners'), __('Banners'));
        return $resultPage;
    }
}
