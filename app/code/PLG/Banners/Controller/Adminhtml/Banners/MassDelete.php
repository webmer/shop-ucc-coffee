<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use PLG\Banners\Model\Banners;

class MassDelete extends MassAction
{
    /**
     * @param Banners $entity
     * @return $this
     */
    protected function massAction(Banners $entity)
    {
        $this->bannersRepository->delete($entity);
        return $this;
    }
}
