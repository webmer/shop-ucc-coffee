<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use PLG\Banners\Controller\Adminhtml\Banners;

class Delete extends Banners
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            try {
                $this->bannersRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The entity has been deleted.'));
                $resultRedirect->setPath('plg_banners/*/');
                return $resultRedirect;
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The author no longer exists.'));
                return $resultRedirect->setPath('plg_banners/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('plg_banners/banners/edit', ['entity_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the entity'));
                return $resultRedirect->setPath('plg_banners/banners/edit', ['entity_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a entity to delete.'));
        $resultRedirect->setPath('plg_banners/*/');
        return $resultRedirect;
    }
}
