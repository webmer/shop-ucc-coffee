<?php
namespace PLG\Banners\Controller\Adminhtml\Banners;

use Magento\Backend\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Framework\View\Result\PageFactory;
use PLG\Banners\Api\BannersRepositoryInterface;
use PLG\Banners\Api\Data\BannersInterface;
use PLG\Banners\Api\Data\BannersInterfaceFactory;
use PLG\Banners\Controller\Adminhtml\Banners;
use PLG\Banners\Model\Uploader;
use PLG\Banners\Model\UploaderPool;

class Save extends Banners
{
    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * @param Registry $registry
     * @param BannersRepositoryInterface $repository
     * @param PageFactory $resultPageFactory
     * @param Date $dateFilter
     * @param Context $context
     * @param BannersInterfaceFactory $bannersFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param UploaderPool $uploaderPool
     */
    public function __construct(
        Registry $registry,
        BannersRepositoryInterface $repository,
        PageFactory $resultPageFactory,
        Date $dateFilter,
        Context $context,
        BannersInterfaceFactory $bannersFactory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        UploaderPool $uploaderPool
    )
    {
        $this->bannersFactory = $bannersFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->uploaderPool = $uploaderPool;
        parent::__construct($registry, $repository, $resultPageFactory, $dateFilter, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        /** @var \PLG\Banners\Api\Data\BannersInterface $entity */
        $entity = null;
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['entity_id']) ? $data['entity_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $entity = $this->bannersRepository->getById((int)$id);
            } else {
                unset($data['entity_id']);
                $entity = $this->bannersFactory->create();
            }
            $avatar = $this->getUploader('image')->uploadFileAndGetName('image', $data);
            $data['image'] = $avatar;
//            $resume = $this->getUploader('file')->uploadFileAndGetName('resume', $data);
//            $data['resume'] = $resume;
            $this->dataObjectHelper->populateWithArray($entity, $data, BannersInterface::class);
            $this->bannersRepository->save($entity);
            $this->messageManager->addSuccessMessage(__('You saved the entity'));
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('plg_banners/banners/edit', ['entity_id' => $entity->getId()]);
            } else {
                $resultRedirect->setPath('plg_banners/banners');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            if ($entity != null) {
                $this->storeBannersDataToSession(
                    $this->dataObjectProcessor->buildOutputDataArray(
                        $entity,
                        BannersInterface::class
                    )
                );
            }
            $resultRedirect->setPath('plg_banners/banners/edit', ['entity_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the entity'));
            if ($entity != null) {
                $this->storeBannersDataToSession(
                    $this->dataObjectProcessor->buildOutputDataArray(
                        $entity,
                        BannersInterface::class
                    )
                );
            }
            $resultRedirect->setPath('plg_banners/banners/edit', ['entity_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param $type
     * @return Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }

    /**
     * @param $entityData
     */
    protected function storeBannersDataToSession($entityData)
    {
        $this->_getSession()->setPLGBannersBannersData($entityData);
    }
}
