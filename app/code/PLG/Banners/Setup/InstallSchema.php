<?php
namespace PLG\Banners\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.Generic.CodeAnalysis.UnusedFunctionParameter)
     */
    // @codingStandardsIgnoreStart
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    // @codingStandardsIgnoreEnd
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('plg_banners_banners')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('plg_banners_banners'));
            $table->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Entity ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable'  => false,],
                    'Name'
                )
                ->addColumn(
                    'from',
                    Table::TYPE_DATE,
                    null,
                    [],
                    'From'
                )
                ->addColumn(
                    'to',
                    Table::TYPE_DATE,
                    null,
                    [],
                    'To'
                )
                ->addColumn(
                    'source',
                    Table::TYPE_TEXT,
                    255,
                    [],
                    'Author Awards'
                )
                ->addColumn(
                    'type',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Author Type'
                )
                ->addColumn(
                    'image',
                    Table::TYPE_TEXT,
                    255,
                    [],
                    'Image'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable'  => false,
                        'default'   => '1',
                    ],
                    'Is Author Active'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Update at'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Creation Time'
                )
                ->setComment('Banners');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('plg_banners_banners'),
                $setup->getIdxName(
                    $installer->getTable('plg_banners_banners'),
                    ['name'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                [
                    'name',
                ],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        //Create Authors to Store table
        if (!$installer->tableExists('plg_banners_banners_store')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('plg_banners_banners_store'));
            $table->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'primary'   => true,
                    ],
                    'Entity ID'
                )
                ->addColumn(
                    'store_id',
                    Table::TYPE_SMALLINT,
                    null,
                    [
                        'unsigned'  => true,
                        'nullable'  => false,
                        'primary'   => true,
                    ],
                    'Store ID'
                )
                ->addIndex(
                    $installer->getIdxName('plg_banners_banners_store', ['store_id']),
                    ['store_id']
                )
                ->addForeignKey(
                    $installer->getFkName('plg_banners_banners_store', 'entity_id', 'plg_banners_banners', 'entity_id'),
                    'entity_id',
                    $installer->getTable('plg_banners_banners'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName('plg_banners_banners_store', 'store_id', 'store', 'store_id'),
                    'store_id',
                    $installer->getTable('store'),
                    'store_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('Banners To Store Link Table');
            $installer->getConnection()->createTable($table);
        }

    }
}
