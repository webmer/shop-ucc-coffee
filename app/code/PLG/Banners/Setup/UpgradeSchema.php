<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 12.04.17
 * Time: 11:16
 */

namespace PLG\Banners\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
//        var_dump(version_compare($context->getVersion(), "1.0.1"), $context->getVersion(), $installer->tableExists('solwin_contactwidget_request'));exit;
        if (version_compare($context->getVersion(), "1.0.1") < 0) {
            $tableName = $installer->getTable('plg_banners_banners');
                if ($installer->getConnection()->isTableExists($tableName) == true) {
                    // Declare data
                    $columns = [
                        'url' => [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'nullable' => true,
                            'comment' => 'URL',
                        ],
                    ];

                    $connection = $installer->getConnection();
                    foreach ($columns as $name => $definition) {
                        $connection->addColumn($tableName, $name, $definition);
                    }

                }
        }

        $installer->endSetup();
    }
}