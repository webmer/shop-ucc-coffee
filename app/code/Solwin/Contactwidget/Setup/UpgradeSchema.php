<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 12.04.17
 * Time: 11:16
 */

namespace Solwin\Contactwidget\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
//        var_dump(version_compare($context->getVersion(), "1.0.4"), $context->getVersion(), $installer->tableExists('solwin_contactwidget_request'));exit;
        if (version_compare($context->getVersion(), "1.0.4") < 0) {
            if (!$installer->tableExists('solwin_contactwidget_request')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('solwin_contactwidget_request')
                )
                    ->addColumn(
                        'entity_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'Entity ID'
                    )
                    ->addColumn(
                        'name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Name'
                    )
                    ->addColumn(
                        'email',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Email'
                    )
                    ->addColumn(
                        'phone_number',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'Phone Number'
                    )
                    ->addColumn(
                        'subject',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Subject'
                    )
                    ->addColumn(
                        'comment',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'What’s on your mind?'
                    )

                    ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [],
                        'Post Created At'
                    )
                    ->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [],
                        'Post Updated At'
                    )
                    ->setComment('Register Table');
                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('solwin_contactwidget_request'),
                    $setup->getIdxName(
                        $installer->getTable('solwin_contactwidget_request'),
                        ['name','email','phone_number','subject','comment'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['name','email','phone_number','subject','comment'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
        }
        if (version_compare($context->getVersion(), "1.0.5") < 0) {
            if (!$installer->tableExists('solwin_contactwidget_request')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('solwin_contactwidget_request')
                )
                    ->addColumn(
                        'firma',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Firmenbezeichnung'
                    )
                    ->addColumn(
                        'surname',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Nachname'
                    )
                    ->addColumn(
                        'address',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'Strasse'
                    )
                    ->addColumn(
                        'postcode',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'PLZ'
                    )
                    ->addColumn(
                        'ort',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Ort'
                    )

                    ->addColumn(
                        'ad_firma',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Firmenbezeichnung'
                    )
                    ->addColumn(
                        'ad_name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Vorname'
                    )
                    ->addColumn(
                        'ad_surname',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Nachname'
                    )
                    ->addColumn(
                        'ad_address',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'Strasse'
                    )
                    ->addColumn(
                        'ad_postcode',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'PLZ'
                    )
                    ->addColumn(
                        'ad_ort',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Ort'
                    )
                    ->addColumn(
                        'ad_phone_number',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'Telefonnummer'
                    )
                    ->addColumn(
                        'ad_email',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'Email'
                    )


                    ->setComment('Register Table');
                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('solwin_contactwidget_request'),
                    $setup->getIdxName(
                        $installer->getTable('solwin_contactwidget_request'),
                        ['firma','surname','address','postcode','ort','ad_firma','ad_name','ad_surname','ad_address','ad_postcode','ad_ort','ad_phone_number','ad_email'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['firma','surname','address','postcode','ort','ad_firma','ad_name','ad_surname','ad_address','ad_postcode','ad_ort','ad_phone_number','ad_email'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
        }
        $installer->endSetup();
    }
}