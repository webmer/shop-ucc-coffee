<?php
/**
 * Sample_News extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Sample
 * @package   Sample_News
 * @copyright 2016 Marius Strajeru
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @author    Marius Strajeru
 */
namespace Solwin\Contactwidget\Controller\Adminhtml\Request;

use Solwin\Contactwidget\Controller\Adminhtml\Request;
use Solwin\Contactwidget\Controller\RegistryConstants;

class Edit extends Request
{
    /**
     * Initialize current author and set it in the registry.
     *
     * @return int
     */
    protected function _initEntity()
    {
        $entityId = $this->getRequest()->getParam('entity_id');
        $this->coreRegistry->register(RegistryConstants::CURRENT_ENTITY_ID, $entityId);

        return $entityId;
    }

    /**
     * Edit or create author
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $entityId = $this->_initEntity();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Solwin_Contactwidget::request');
        $resultPage->getConfig()->getTitle()->prepend(__('Entities'));
        $resultPage->addBreadcrumb(__('News'), __('News'));
        $resultPage->addBreadcrumb(__('Entities'), __('Entities'), $this->getUrl('solwin_contactwidget/request'));

        if ($entityId === null) {
            $resultPage->addBreadcrumb(__('New Entity'), __('New Entity'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Entity'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Entity'), __('Edit Entity'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->requestRepository->getById($entityId)->getName()
            );
        }
        return $resultPage;
    }
}
