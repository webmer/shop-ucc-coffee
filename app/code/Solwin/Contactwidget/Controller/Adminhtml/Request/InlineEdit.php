<?php
/**
 * Sample_News extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Sample
 * @package   Sample_News
 * @copyright 2016 Marius Strajeru
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @author    Marius Strajeru
 */
namespace Solwin\Contactwidget\Controller\Adminhtml\Request;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Framework\View\Result\PageFactory;
use Solwin\Contactwidget\Api\RequestRepositoryInterface as CRepositoryInterface;
use Solwin\Contactwidget\Api\Data\RequestInterface as CInterface;
use Solwin\Contactwidget\Api\Data\RequestInterfaceFactory as CInterfaceFactory;
use Solwin\Contactwidget\Controller\Adminhtml\Request as CController;
use Solwin\Contactwidget\Model\Request as CModel;
use Solwin\Contactwidget\Model\ResourceModel\Request as CResourceModel;

class InlineEdit extends CController
{
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CResourceModel
     */
    protected $resourceModel;

    /**
     * @param Registry $registry
     * @param CRepositoryInterface $repository
     * @param PageFactory $resultPageFactory
     * @param Date $dateFilter
     * @param Context $context
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonFactory $jsonFactory
     * @param CResourceModel $resourceModel
     */
    public function __construct(
        Registry $registry,
        CRepositoryInterface $repository,
        PageFactory $resultPageFactory,
        Date $dateFilter,
        Context $context,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        JsonFactory $jsonFactory,
        CResourceModel $resourceModel
    )
    {
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->jsonFactory         = $jsonFactory;
        $this->resourceModel = $resourceModel;
        parent::__construct($registry, $repository, $resultPageFactory, $dateFilter, $context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $entityId) {
            /** @var CInterface $entity */
            $entity = $this->requestRepository->getById((int)$entityId);
            try {
                $data = $this->filterData($postItems[$entityId]);
                $this->dataObjectHelper->populateWithArray($entity, $data , CInterface::class);
                $this->resourceModel->saveAttribute($entity, array_keys($data));
            } catch (LocalizedException $e) {
                $messages[] = $this->getErrorWithEntityId($entity, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithEntityId($entity, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithEntityId(
                    $entity,
                    __('Something went wrong while saving the entity.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add author id to error message
     *
     * @param CModel $entity
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithEntityId(CModel $entity, $errorText)
    {
        return '[Entity ID: ' . $entity->getId() . '] ' . $errorText;
    }
}
