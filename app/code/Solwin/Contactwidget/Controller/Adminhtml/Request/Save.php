<?php
/**
 * Sample_News extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Sample
 * @package   Sample_News
 * @copyright 2016 Marius Strajeru
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @author    Marius Strajeru
 */
namespace Solwin\Contactwidget\Controller\Adminhtml\Author;

use Magento\Backend\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Framework\View\Result\PageFactory;
use Solwin\Contactwidget\Api\RequestRepositoryInterface as CRepositoryInterface;
use Solwin\Contactwidget\Api\Data\RequestInterface as CInterface;
use Solwin\Contactwidget\Api\Data\RequestInterfaceFactory as CInterfaceFactory;
use Solwin\Contactwidget\Controller\Adminhtml\Request;

class Save extends Request
{
    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    protected $factory;

    /**
     * @param Registry $registry
     * @param CRepositoryInterface $repository
     * @param PageFactory $resultPageFactory
     * @param Date $dateFilter
     * @param Context $context
     * @param CInterfaceFactory $factory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        Registry $registry,
        CRepositoryInterface $repository,
        PageFactory $resultPageFactory,
        Date $dateFilter,
        Context $context,
        CInterfaceFactory $factory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper
    )
    {
        $this->factory = $factory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($registry, $repository, $resultPageFactory, $dateFilter, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        /** @var CInterface $entity */
        $entity = null;
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['entity_id']) ? $data['entity_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $entity = $this->requestRepository->getById((int)$id);
            } else {
                unset($data['entity_id']);
                $entity = $this->factory->create();
            }
//            $avatar = $this->getUploader('image')->uploadFileAndGetName('avatar', $data);
//            $data['avatar'] = $avatar;
//            $resume = $this->getUploader('file')->uploadFileAndGetName('resume', $data);
//            $data['resume'] = $resume;
            $this->dataObjectHelper->populateWithArray($entity, $data, CInterface::class);
            $this->requestRepository->save($entity);
            $this->messageManager->addSuccessMessage(__('You saved the entity'));
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('solwin_contactwidget/request/edit', ['entity_id' => $entity->getId()]);
            } else {
                $resultRedirect->setPath('solwin_contactwidget/request');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            if ($entity != null) {
                $this->storeEntityDataToSession(
                    $this->dataObjectProcessor->buildOutputDataArray(
                        $entity,
                        CInterface::class
                    )
                );
            }
            $resultRedirect->setPath('solwin_contactwidget/request/edit', ['entity_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the entity'));
            if ($entity != null) {
                $this->storeEntityDataToSession(
                    $this->dataObjectProcessor->buildOutputDataArray(
                        $entity,
                        CInterface::class
                    )
                );
            }
            $resultRedirect->setPath('sample_news/author/edit', ['author_id' => $id]);
        }
        return $resultRedirect;
    }


    /**
     * @param $authorData
     */
    protected function storeEntityDataToSession($data)
    {
        $this->_getSession()->setSolwinContactwidgetRequestData($data);
    }
}
