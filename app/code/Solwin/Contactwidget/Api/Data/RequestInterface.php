<?php
/**
 * Sample_News extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Sample
 * @package   Sample_News
 * @copyright 2016 Marius Strajeru
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @author    Marius Strajeru
 */
namespace Solwin\Contactwidget\Api\Data;

/**
 * @api
 */
interface RequestInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const AUTHOR_ID         = 'entity_id';
    const FIRMA             = 'firma';
    const ADDRESS           = 'address';
    const POSTCODE          = 'postcode';
    const ORT               = 'ort';
    const LAST_NAME         = 'last_name';
    const NAME              = 'name';
    const EMAIL             = 'email';
    const PHONE_NUMBER      = 'phone_number';

    const AD_FIRMA             = 'ad_firma';
    const AD_ADDRESS           = 'ad_address';
    const AD_POSTCODE          = 'ad_postcode';
    const AD_ORT               = 'ad_ort';
    const AD_LAST_NAME         = 'ad_last_name';
    const AD_NAME              = 'ad_name';
    const AD_EMAIL             = 'ad_email';
    const AD_PHONE_NUMBER      = 'ad_phone_number';

    const SUBJECT           = 'subject';
    const COMMENT           = 'comment';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';


    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string
     */
    public function getFirma();
    public function getAdFirma();

    /**
     * Get name
     *
     * @return string
     */
    public function getLastName();
    public function getAdLastName();

    /**
     * Get name
     *
     * @return string
     */
    public function getAddress();
    public function getAdAddress();

    /**
     * Get name
     *
     * @return string
     */
    public function getPostcode();
    public function getAdPostcode();

    /**
     * Get name
     *
     * @return string
     */
    public function getOrt();
    public function getAdOrt();

    /**
     * Get name
     *
     * @return string
     */
    public function getName();
    public function getAdName();

    /**
     * Get url key
     *
     * @return string
     */
    public function getEmail();
    public function getAdEmail();

    /**
     * Get is active
     *
     * @return bool|int
     */
    public function getPhoneNumber();
    public function getAdPhoneNumber();

    /**
     * Get in rss
     *
     * @return bool|int
     */
    public function getSubject();

    /**
     * Get biography
     *
     * @return string
     */
    public function getComment();



    /**
     * set id
     *
     * @param $id
     * @return self
     */
    public function setId($id);

    /**
     * Set firma
     *
     * @param $firma
     * @return self
     */
    public function setFirma($firma);
    public function setAdFirma($firma);

    /**
     * set last_name
     *
     * @param $last_name
     * @return self
     */
    public function setLastName($last_name);
    public function setAdLastName($last_name);

    /**
     * set address
     *
     * @param $address
     * @return self
     */
    public function setAddress($address);
    public function setAdAddress($address);

    /**
     * Set postcode
     *
     * @param $postcode
     * @return self
     */
    public function setPostcode($postcode);
    public function setAdPostcode($postcode);

    /**
     * Set name
     *
     * @param $ort
     * @return self
     */
    public function setOrt($ort);
    public function setAdOrt($ort);

    /**
     * set name
     *
     * @param $name
     * @return self
     */
    public function setName($name);
    public function setAdName($name);

    /**
     * set url key
     *
     * @param $email
     * @return self
     */
    public function setEmail($email);
    public function setAdEmail($email);

    /**
     * Set is active
     *
     * @param $phoneNumber
     * @return self
     */
    public function setPhoneNumber($phoneNumber);
    public function setAdPhoneNumber($phoneNumber);

    /**
     * Set in rss
     *
     * @param $subject
     * @return self
     */
    public function setSubject($subject);

    /**
     * Set biography
     *
     * @param $comment
     * @return self
     */
    public function setComment($comment);



    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * set created at
     *
     * @param $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * set updated at
     *
     * @param $updatedAt
     * @return self
     */
    public function setUpdatedAt($updatedAt);

}
