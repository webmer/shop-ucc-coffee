<?php

namespace Solwin\Contactwidget\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Data\Collection\Db;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Solwin\Contactwidget\Api\Data\RequestInterface;
use Solwin\Contactwidget\Model\Request\Url;
use Solwin\Contactwidget\Model\ResourceModel\Request as RequestResourceModel;
use Solwin\Contactwidget\Model\Routing\RoutableInterface;
use Solwin\Contactwidget\Model\Source\AbstractSource;


/**
 * @method RequestResourceModel _getResource()
 * @method RequestResourceModel getResource()
 */
class Request extends AbstractModel implements RequestInterface, RoutableInterface
{
    /**
     * @var int
     */
    const STATUS_ENABLED = 1;
    /**
     * @var int
     */
    const STATUS_DISABLED = 0;
    /**
     * @var Url
     */
    protected $urlModel;
    /**
     * cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'solwin_contactwidget_request';

    /**
     * cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'solwin_contactwidget_request';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'solwin_contactwidget_request';

    /**
     * filter model
     *
     * @var \Magento\Framework\Filter\FilterManager
     */
    protected $filter;

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * @var \Solwin\Contactwidget\Model\Output
     */
    protected $outputProcessor;

    /**
     * @var AbstractSource[]
     */
    protected $optionProviders;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param Output $outputProcessor
     * @param UploaderPool $uploaderPool
     * @param FilterManager $filter
     * @param Url $urlModel
     * @param array $optionProviders
     * @param array $data
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Output $outputProcessor,
        FilterManager $filter,
        Url $urlModel,
        array $optionProviders = [],
        array $data = [],
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null
    )
    {
        $this->outputProcessor = $outputProcessor;
        $this->filter          = $filter;
        $this->urlModel        = $urlModel;
        $this->optionProviders = $optionProviders;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(RequestResourceModel::class);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(RequestInterface::NAME);
    }

    /**
     * Get firma
     *
     * @return string
     */
    public function getFirma()
    {
        return $this->getData(RequestInterface::FIRMA);
    }

    /**
     * Get ad_firma
     *
     * @return string
     */
    public function getAdFirma()
    {
        return $this->getData(RequestInterface::AD_FIRMA);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->getData(RequestInterface::LAST_NAME);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAdLastName()
    {
        return $this->getData(RequestInterface::AD_LAST_NAME);
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->getData(RequestInterface::ADDRESS);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAdAddress()
    {
        return $this->getData(RequestInterface::AD_ADDRESS);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->getData(RequestInterface::POSTCODE);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAdPostcode()
    {
        return $this->getData(RequestInterface::AD_POSTCODE);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getOrt()
    {
        return $this->getData(RequestInterface::ORT);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAdOrt()
    {
        return $this->getData(RequestInterface::AD_ORT);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAdName()
    {
        return $this->getData(RequestInterface::AD_NAME);
    }

    /**
     * Get in rss
     *
     * @return bool|int
     */
    public function getEmail()
    {
        return $this->getData(RequestInterface::EMAIL);
    }

    /**
     * Get in rss
     *
     * @return bool|int
     */
    public function getAdEmail()
    {
        return $this->getData(RequestInterface::AD_EMAIL);
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->getData(RequestInterface::PHONE_NUMBER);
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getAdPhoneNumber()
    {
        return $this->getData(RequestInterface::AD_PHONE_NUMBER);
    }

    /**
     * Get awards
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->getData(RequestInterface::SUBJECT);
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getComment()
    {
        return $this->getData(RequestInterface::COMMENT);
    }

    /**
     * set firma
     *
     * @param $firma
     * @return RequestInterface
     */
    public function setFirma($firma)
    {
        return $this->setData(RequestInterface::FIRMA, $firma);
    }

    /**
     * set firma
     *
     * @param $firma
     * @return RequestInterface
     */
    public function setAdFirma($firma)
    {
        return $this->setData(RequestInterface::AD_FIRMA, $firma);
    }

    /**
     * set name
     *
     * @param $name
     * @return RequestInterface
     */
    public function setName($name)
    {
        return $this->setData(RequestInterface::NAME, $name);
    }

    /**
     * set name
     *
     * @param $name
     * @return RequestInterface
     */
    public function setAdName($name)
    {
        return $this->setData(RequestInterface::AD_NAME, $name);
    }

    /**
     * set lastName
     *
     * @param $lastName
     * @return RequestInterface
     */
    public function setLastName($lastName)
    {
        return $this->setData(RequestInterface::LAST_NAME, $lastName);
    }

    /**
     * set lastName
     *
     * @param $lastName
     * @return RequestInterface
     */
    public function setAdLastName($lastName)
    {
        return $this->setData(RequestInterface::AD_LAST_NAME, $lastName);
    }


    /**
     * set address
     *
     * @param $address
     * @return RequestInterface
     */
    public function setAddress($address)
    {
        return $this->setData(RequestInterface::ADDRESS, $address);
    }

    /**
     * set address
     *
     * @param $address
     * @return RequestInterface
     */
    public function setAdAddress($address)
    {
        return $this->setData(RequestInterface::AD_ADDRESS, $address);
    }

    /**
     * set postcode
     *
     * @param $postcode
     * @return RequestInterface
     */
    public function setPostcode($postcode)
    {
        return $this->setData(RequestInterface::POSTCODE, $postcode);
    }

    /**
     * set postcode
     *
     * @param $postcode
     * @return RequestInterface
     */
    public function setAdPostcode($postcode)
    {
        return $this->setData(RequestInterface::AD_POSTCODE, $postcode);
    }

    /**
     * set ort
     *
     * @param $ort
     * @return RequestInterface
     */
    public function setOrt($ort)
    {
        return $this->setData(RequestInterface::POSTCODE, $ort);
    }

    /**
     * set ort
     *
     * @param $ort
     * @return RequestInterface
     */
    public function setAdOrt($ort)
    {
        return $this->setData(RequestInterface::AD_ORT, $ort);
    }

    /**
     * Set in rss
     *
     * @param $email
     * @return RequestInterface
     */
    public function setEmail($email)
    {
        return $this->setData(RequestInterface::EMAIL, $email);
    }

    /**
     * Set email
     *
     * @param $email
     * @return RequestInterface
     */
    public function setAdEmail($email)
    {
        return $this->setData(RequestInterface::AD_EMAIL, $email);
    }

    /**
     * Set phoneNumber
     *
     * @param $phone
     * @return RequestInterface
     */
    public function setPhoneNumber($phone)
    {
        return $this->setData(RequestInterface::PHONE_NUMBER, $phone);
    }

    /**
     * Set phoneNumber
     *
     * @param $phone
     * @return RequestInterface
     */
    public function setAdPhoneNumber($phone)
    {
        return $this->setData(RequestInterface::AD_PHONE_NUMBER, $phone);
    }

    /**
     * Set DOB
     *
     * @param $subject
     * @return RequestInterface
     */
    public function setSubject($subject)
    {
        return $this->setData(RequestInterface::SUBJECT, $subject);
    }

    /**
     * set type
     *
     * @param $comment
     * @return RequestInterface
     */
    public function setComment($comment)
    {
        return $this->setData(RequestInterface::COMMENT, $comment);
    }


    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(RequestInterface::CREATED_AT);
    }

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(RequestInterface::UPDATED_AT);
    }


    /**
     * set created at
     *
     * @param $createdAt
     * @return RequestInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(RequestInterface::CREATED_AT, $createdAt);
    }

    /**
     * set updated at
     *
     * @param $updatedAt
     * @return RequestInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(RequestInterface::UPDATED_AT, $updatedAt);
    }


    /**
     * Check if author url key exists
     * return author id if author exists
     *
     * @param string $urlKey
     * @param int $storeId
     * @return int
     */
    public function checkUrlKey($urlKey, $storeId)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $storeId);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }



    /**
     * sanitize the url key
     *
     * @param $string
     * @return string
     */
    public function formatUrlKey($string)
    {
        return $this->filter->translitUrl($string);
    }

    /**
     * @return mixed
     */
    public function getRequestUrl()
    {
        return $this->urlModel->getRequestUrl($this);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool)$this->getIsActive();
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeText($attribute)
    {
        if (!isset($this->optionProviders[$attribute])) {
            return '';
        }
        if (!($this->optionProviders[$attribute] instanceof AbstractSource)) {
            return '';
        }
        return $this->optionProviders[$attribute]->getOptionText($this->getData($attribute));
    }
}
