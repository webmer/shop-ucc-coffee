<?php

namespace Solwin\Contactwidget\Model\Request;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Solwin\Contactwidget\Model\Request;

class Url
{
    /**
     * @var string
     */
    const LIST_URL_CONFIG_PATH      = 'solwin_contactwidget/request/list_url';
    /**
     * @var string
     */
    const URL_PREFIX_CONFIG_PATH    = 'solwin_contactwidget/request/url_prefix';
    /**
     * @var string
     */
    const URL_SUFFIX_CONFIG_PATH    = 'solwin_contactwidget/request/url_suffix';
    /**
     * url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param UrlInterface $urlBuilder
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        UrlInterface $urlBuilder,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getListUrl()
    {
        $sefUrl = $this->scopeConfig->getValue(self::LIST_URL_CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if ($sefUrl) {
            return $this->urlBuilder->getUrl('', ['_direct' => $sefUrl]);
        }
        return $this->urlBuilder->getUrl('solwin_contactwidget/request/index');
    }

    /**
     * @param Request $entity
     * @return string
     */
    public function getAuthorUrl(Request $entity)
    {
        if ($urlKey = $entity->getUrlKey()) {
            $prefix = $this->scopeConfig->getValue(
                self::URL_PREFIX_CONFIG_PATH,
                ScopeInterface::SCOPE_STORE
            );
            $suffix = $this->scopeConfig->getValue(
                self::URL_SUFFIX_CONFIG_PATH,
                ScopeInterface::SCOPE_STORE
            );
            $path = (($prefix) ? $prefix . '/' : '').
                $urlKey .
                (($suffix) ? '.'. $suffix : '');
            return $this->urlBuilder->getUrl('', ['_direct'=>$path]);
        }
        return $this->urlBuilder->getUrl('solwin_contactwidget/request/view', ['id' => $entity->getId()]);
    }
}
