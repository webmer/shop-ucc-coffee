<?php
namespace Solwin\Contactwidget\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Solwin\Contactwidget\Api\RequestRepositoryInterface;
use Solwin\Contactwidget\Api\Data;
use Solwin\Contactwidget\Api\Data\RequestInterface;
use Solwin\Contactwidget\Api\Data\RequestInterfaceFactory;
use Solwin\Contactwidget\Api\Data\RequestSearchResultsInterfaceFactory;
use Solwin\Contactwidget\Model\ResourceModel\Request as CResource;
use Solwin\Contactwidget\Model\ResourceModel\Request\Collection;
use Solwin\Contactwidget\Model\ResourceModel\Request\CollectionFactory as CCollectionFactory;

/**
 * Class AuthorRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RequestRepository implements RequestRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];
    /**
     * @var CResource
     */
    protected $resource;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var RequestCollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var RequestSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var RequestInterfaceFactory
     */
    protected $interfaceFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    public function __construct(
        CResource $resource,
        StoreManagerInterface $storeManager,
        CCollectionFactory $collectionFactory,
        RequestSearchResultsInterfaceFactory $authorSearchResultsInterfaceFactory,
        RequestInterfaceFactory $interfaceFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->resource                 = $resource;
        $this->storeManager             = $storeManager;
        $this->collectionFactory  = $collectionFactory;
        $this->searchResultsFactory     = $authorSearchResultsInterfaceFactory;
        $this->interfaceFactory   = $interfaceFactory;
        $this->dataObjectHelper         = $dataObjectHelper;
    }
    /**
     * Save page.
     *
     * @param RequestInterface $author
     * @return RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(RequestInterface $author)
    {
        /** @var RequestInterface|\Magento\Framework\Model\AbstractModel $author */
        try {
            $this->resource->save($author);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the author: %1',
                $exception->getMessage()
            ));
        }
        return $author;
    }

    /**
     * Retrieve Author.
     *
     * @param int $authorId
     * @return RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($authorId)
    {
        if (!isset($this->instances[$authorId])) {
            /** @var RequestInterface|\Magento\Framework\Model\AbstractModel $author */
            $author = $this->interfaceFactory->create();
            $this->resource->load($author, $authorId);
            if (!$author->getId()) {
                throw new NoSuchEntityException(__('Requested author doesn\'t exist'));
            }
            $this->instances[$authorId] = $author;
        }
        return $this->instances[$authorId];
    }

    /**
     * Retrieve pages matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return RequestSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var RequestSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'entity_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var RequestInterface[] $authors */
        $authors = [];
        /** @var Request $author */
        foreach ($collection as $author) {
            /** @var RequestInterface $authorDataObject */
            $authorDataObject = $this->interfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($authorDataObject, $author->getData(), RequestInterface::class);
            $authors[] = $authorDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($authors);
    }

    /**
     * Delete author.
     *
     * @param RequestInterface $author
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(RequestInterface $author)
    {
        /** @var RequestInterface|\Magento\Framework\Model\AbstractModel $author */
        $id = $author->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($author);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove author %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete author by ID.
     *
     * @param int $authorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($authorId)
    {
        $author = $this->getById($authorId);
        return $this->delete($author);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

}
